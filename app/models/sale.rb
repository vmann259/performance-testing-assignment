class Sale < ActiveRecord::Base
  belongs_to :customer

  amount = Sale.includes(:amount)
  sale_tax = Sale.includes(:tax)


  def amount_with_tax
    self.amount + self.tax
  end
end
